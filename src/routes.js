import Home from './components/Home.vue';
import Table from './components/Table.vue';


const routes = [
   { path: '/', component: Home },
   { path: '/table', component: Table }
];

export default routes;
