import UserProvider from './../../Provider/modules/user';

const
    state = {
      user: null,
      register: null,
      values: {}
    };

// getters
const getters = {
  getUser: (state) => state.user,
  getRegister: (state) => state.register,
  getBets: (state) => state.values
};

// actions
const actions = {
  setBets({state, commit}, values) {
    commit('SET_BETS', values)
    console.log(state.values);
  },

  getTest({state, commit}, form) {
    return UserProvider.getTest(form);
  },

  roomOne({state, commit}, form) {
    return UserProvider.roomOne(form);
  },

  login({state, commit}, form) {
    return UserProvider.login(form);
  },

  register({state, commit}){
    return UserProvider.register(state.register)
  },

  setRegister({commit}, form){
    commit('SET_REGISTER', form)
  },

  initUser({state, commit}){
    return new Promise((resolve, reject) => {
      UserProvider.getCurrentUser()
        .then((user) => {
          console.log(user)
          commit('INIT_USER', user);
          resolve(user);
        })
        .catch(reject);
    });
  }
};

// mutations
const mutations = {
  INIT_USER(state, user){
    state.user = user;
  },

  SET_REGISTER(state, form){
    state.register = form;
  },

  SET_BETS(state, bets){
    state.values = {
      bets: {
        split_1: 10,
      },
      nameRoom: 'light',
    };
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
