import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import User from './modules/user';

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    User
  },

  strict: debug
});
