

data: {
    favorites: [],
    matchesByEntity: [
        {
            id: 1,
            title: 'Football',
            img: 'dfh/dfh.fg',
            count: 20,
            views: 23,
            matches: [
                {
                    id: 1,
                    country: 'RU',
                    name: 'FNL CUP. CYPRUS1',
                },
                {
                    country: 'UA',
                    name: 'FNL CUP. CYPRUS5',
                }
                {
                    country: 'US',
                    name: 'FNL CUP. CYPRUS45',
                }
                {
                    country: 'TR',
                    name: 'FNL CUP. CYPRUSdsf',
                }
            ],
        },
        {
            id: 2,
            title: 'Basketbal',
            img: 'dfh/dfh.fg',
            count: 20,
            views: 23,
            matches: [
                {
                    id: 456,
                    country: 'RU',
                    name: 'FNL CUP. CYPRUS1',
                },
                {
                    id: 345,
                    country: 'UA',
                    name: 'FNL CUP. CYPRUS5',
                }
                {
                    id: 345,
                    country: 'US',
                    name: 'FNL CUP. CYPRUS45',
                }
                {
                    id: 345,
                    country: 'TR',
                    name: 'FNL CUP. CYPRUSdsf',
                }
            ],
        }
    ],

    methods: {
        addToFavorite: function(event, match) {
            this.favorites.push(match);
        },

        removeFromFavorite: function(event, match, index) {
            confirm(`Вы действительо хотите удалить ${match.name}?`, function() {
                this.favorites.splice(index, 1);
            });
        }
    }
}

<div v-for="entity in matchesByEntity" :key="match.id">
    <div v-text="entity.title"></div>
    <ul>
        <li v-for="match in entity.matches" :key="match.id">
            <span class="flag" :class="match.country"></span>
            <span v-text="match.name"></span>
            <span @click="addToFavorite($event, match)">+</span>
        </li>
    <ul>
<div>

<ul v-if="favorites.length">
    <li v-for="(match, index) in entity.matches" :key="match.id">
        <span class="flag" :class="match.country"><span>
        <span v-text="match.name"><span>
        <span @click="removeFromFavorite($event, match, index)">-<span>
    </li>
<ul>
<div v-else>no favorite data</div>


<span @click="addToFavorite($event, match, entity)">+<span>

api.get('/api/entiry/match', {date: 3453425}).then((response) => {
    this.matchesByEntity = response.data;
}).then(() => {
    this.fireApp();
});
