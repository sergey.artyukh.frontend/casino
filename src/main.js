import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import store from './Store/index'
import axios from 'axios'
import VueAxios from 'vue-axios';
import './Provider/index';
// import Pusher from 'pusher-js'

import './components'
// import './laravel-echo.js'
import VModal from 'vue-js-modal'
import SmartTable from 'vuejs-smart-table'
import routes from './routes.js';


import Echo from 'laravel-echo';
import Pusher from 'pusher-js';


// const socket = new Pusher('36be52c18aae69126cbb', {
//   cluster: APP_CLUSTER,
//
// });

// window.Echo = new Echo({
// 	broadcaster: 'pusher',
// 	key: '36be52c18aae69126cbb',
// 	cluster: 'eu',
//   auth: {
//     headers: {
//       authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xOTIuMTY4LjY4LjEyNFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU2MDQzNjg5MSwiZXhwIjoxNTYwNDQwNDkxLCJuYmYiOjE1NjA0MzY4OTEsImp0aSI6IndNVDdaNjdpc25RWDJ3YTYiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.qHfVS-nvC9IzYz9KTtOK4dJuPR5VDTmXWYc_dIhd-tQ'
//     },
//   },
//   authEndpoint: 'http://192.168.68.124:80/api/auth/auth-echo-laravel',
// 	encrypted: true
// });

// auth: {
//   headers: {
//     authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xOTIuMTY4LjY4LjEyNFwvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU2MDM1NjM0NiwiZXhwIjoxNTYwMzU5OTQ2LCJuYmYiOjE1NjAzNTYzNDYsImp0aSI6ImtKRWtYaDh2cVlFczhXSXAiLCJzdWIiOjYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.QmVwTlGTS-_oZxzeGC872QitGPY_8IB1utI_LG9NmG4'
//   }
// },

// import { library } from '@fortawesome/fontawesome-svg-core'
// import { fafacebook } from '@fortawesome/free-solid-svg-icons'
// import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
//
// library.add(fafacebook)
//
// Vue.component('font-awesome-icon', FontAwesomeIcon)


// Vue.use(LaravelEcho)
Vue.use(VueAxios, axios)
Vue.use(SmartTable)
Vue.use(VModal)
Vue.use(VueRouter);


const eventBus = new Vue();
const router = new VueRouter({routes});

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
