import Provider from "../";

export default {

  getTest: () => {
    return new Promise((resolve, reject) => {
      Provider.api.get("/start/1")
          .then((response) => {
            // Provider.setBearer(response.data.access_token);
            resolve();
          })
          .catch((error) => reject(error));
    });
  },

  roomOne: (values) => {
    return new Promise((resolve, reject) => {
      Provider.api.post("/room-one", values)
          .then((response) => {
            let responseMoney = response.data.money;
            console.log(responseMoney);
            resolve();
          })
          .catch((error) => reject(error));

    });
  },

  login: (form) => {
    return new Promise((resolve, reject) => {
      Provider.api.post("auth/login", form)
          .then((response) => {
            Provider.setBearer(response.data.access_token);
            resolve();
          })
          .catch((error) => reject(error));
    });
  },

  getCurrentUser: () => {
    return new Promise((resolve, reject) => {
      Provider.api.post("auth/me")
          .then((response) => {
            resolve(response.data);
          })
          .catch((error) => reject(error));
    });
  },

  register: (form) => {
    return new Promise((resolve, reject) => {
      Provider.api.post("auth/register", form)
          .then((response) => {
            Provider.setBearer(response.data.access_token);
            resolve();
          })
          .catch((error) => reject(error));
    });
  }
}
