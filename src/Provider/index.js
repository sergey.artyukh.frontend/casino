import axios from "axios";
import Echo from 'laravel-echo';
import Pusher from 'pusher-js';

const PREFIX = process.env.VUE_APP_API_PREFIX || 'api/';
const BASE_URL = process.env.VUE_APP_API_URL || `http://192.168.68.124:80/${PREFIX}`;
const TOKEN_KEY = process.env.VUE_APP_TOKEN_KEY || "br_token";

// $.ajax({
//     url: url,
//     dataType : 'jsonp',
//     beforeSend : function(xhr) {
//       // set header if JWT is set
//       if ($window.sessionStorage.token) {
//           xhr.setRequestHeader("Authorization", "Bearer " +  $window.sessionStorage.token);
//       }
//
//     },
//     error : function() {
//       // error handler
//     },
//     success: function(data) {
//         // success handler
//     }
// });

class Api {
  constructor(options) {
    this.PREFIX = options.PREFIX;
    this.BASE_URL = options.BASE_URL;
    this.TOKEN_KEY = options.TOKEN_KEY;
    this.api = this.getInstance();
  }

  getToken() {
    return window.localStorage.getItem(this.TOKEN_KEY);
  }

  setBearer(newToken) {
    localStorage.setItem(this.TOKEN_KEY, newToken);
    this.api = this.getInstance();
  }

  getInstance() {
    const token = this.getToken();
    this.getTest();
    return axios.create({
      baseURL: this.BASE_URL,
      headers: {
        "Content-Type": "application/json",
        "Authorization": token ? `Bearer ${token}` : ''
      }
    });
  }

  getTest() {
    const token = this.getToken();

    return window.Echo = new Echo({
    	broadcaster: 'pusher',
    	key: '36be52c18aae69126cbb',
    	cluster: 'eu',
      auth: {
        headers: {
          authorization: token ? `Bearer ${token}` : ''
        },
      },
      authEndpoint: 'http://192.168.68.124:80/api/auth/auth-echo-laravel',
    	encrypted: true
    });
  }
}

export default new Api({PREFIX, BASE_URL, TOKEN_KEY});
