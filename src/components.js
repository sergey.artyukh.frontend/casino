import Vue from 'vue';

import UiButton from './ui-components/ui-button/ui-button.vue';
import UiChest from './ui-components/ui-chest/ui-chest.vue';
import UiSignInModal from './ui-components/ui-signin-modal/ui-signin-modal.vue';
import UiSignUpModal from './ui-components/ui-signup-modal/ui-signup-modal.vue';
import UiAccount from './ui-components/ui-account/ui-account.vue';
import UiAccountTabs from './ui-components/ui-account__tabs/ui-account__tabs.vue';
import UiPersonalData from './ui-components/ui-personal-data/ui-personal-data.vue';
import UiAccountRefill from './ui-components/ui-account-refill/ui-account-refill.vue';
import UiRefillHistory from './ui-components/ui-refill-history/ui-refill-history.vue';
import UiWithdraw from './ui-components/ui-withdraw/ui-withdraw.vue';
import UiModalCloseButton from './ui-components/ui-modal-close-button/ui-modal-close-button.vue';
import UiDivider from './ui-components/ui-divider/ui-divider.vue';
import UiInput from './ui-components/ui-input/ui-input.vue';
import UiTermsModal from './ui-components/ui-terms-modal/ui-terms.vue';
import UiResultArea from './ui-components/ui-result-area/ui-result-area.vue';
import UiBetTable from './ui-components/ui-bet-table/ui-bet-table.vue';
import UiBetChips from './ui-components/ui-bet-chips/ui-bet-chips.vue';
import UiOvalBet from './ui-components/ui-oval-bet/ui-oval-bet.vue';

import Test from './apiVUE.vue';




import Home from './components/Home.vue';
// import NavBar from './components/nav-bar.vue';




Vue.component('test', Test);
Vue.component('ui-oval-bet', UiOvalBet);
Vue.component('ui-bet-chips', UiBetChips);
Vue.component('ui-bet-table', UiBetTable);
Vue.component('ui-result-area', UiResultArea);
Vue.component('ui-terms', UiTermsModal);
Vue.component('ui-input', UiInput);
Vue.component('ui-divider', UiDivider);
Vue.component('ui-modal-close-button', UiModalCloseButton);
Vue.component('ui-withdraw', UiWithdraw);
Vue.component('ui-refill-history', UiRefillHistory);
Vue.component('ui-account-refill', UiAccountRefill);
Vue.component('ui-personal-data', UiPersonalData);
Vue.component('ui-account-tabs', UiAccountTabs);
Vue.component('ui-account', UiAccount);
Vue.component('ui-signup-modal', UiSignUpModal);
Vue.component('ui-signin-modal', UiSignInModal);
Vue.component('ui-button', UiButton);
Vue.component('ui-chest', UiChest);




Vue.component('home', Home);
// Vue.component('nav-bar', NavBar);
